#!/usr/bin/env bash
#
# This script runs linters against all files staged for a commit
#

CWD=$(pwd)
# shellcheck source=./docker/spinner.sh
source "${CWD}/spinner.sh"

cd "/workspace/repo" || exit 1

if ! git rev-parse --git-dir > /dev/null 2>&1; then
    echo "Not at the root of a git repository, exiting..."
    exit 1
fi

nc="\033[0m"
red="\033[0;31m"
green="\033[0;32m"
yellow="\033[0;33m"

branch_name=$(git rev-parse --abbrev-ref HEAD)
staged_files=$(git diff-index --name-only --diff-filter=A --cached HEAD --)

global_status=0

json_lint () {
    local status=0
    local logging=""

    json_files=$(echo "${1}" | grep -P "\.json$")
    for file in ${json_files}; do
        output=$(python -m json.tool "${file}" 2>&1) || result=$?
        if [ -n "$result" ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${json_files}" ]; then
        echo -e "json...............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "json...............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "json...............................................${green}Passed${nc}"
    fi
}

yaml_lint () {
    local status=0
    local logging=""

    yaml_files=$(echo "${1}" | grep -P "\.((yaml)|(yml))$")
    for file in ${yaml_files}; do
        output=$(yamllint "${file}" 2>&1)
        if [ -n "$output" ]; then
          logging=$(echo -e "${red} - ${file}${nc}\n${output}")
          status=1
        fi
    done

    if [ -z "${yaml_files}" ]; then
        echo -e "yaml...............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "yaml...............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "yaml...............................................${green}Passed${nc}"
    fi
}

shell_lint () {
    local status=0
    local logging=""

    shell_files=$(echo "${1}" | grep -P "\.sh$")
    for file in ${shell_files}; do
        output=$(shellcheck -x "$file" 2>&1) || result=$?
        if [ -n "$result" ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${shell_files}" ]; then
        echo -e "shellcheck.........................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "shellcheck.........................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "shellcheck.........................................${green}Passed${nc}"
    fi
}

go_lint () {
    local status=0
    local logging=""

    go_files=$(echo "${1}" | grep -P "\.go$")
    for file in ${go_files}; do
        output=$(golint "$file" 2>&1)
        if [ -n "$output" ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${go_files}" ]; then
        echo -e "golint.............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "golint.............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "golint.............................................${red}Passed${nc}"
    fi
}

es_lint () {
    local status=0
    local logging=""

    js_files=$(echo "${1}" | grep -P "\.js$")
    for file in ${js_files}; do
        output=$(eslint "$file" 2>&1) || result=$?
        if [ "$result" -ne 0 ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${js_files}" ]; then
        echo -e "eslint.............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "eslint.............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "eslint.............................................${red}Passed${nc}"
    fi
}

tf_lint () {
    local status=0
    local logging=""

    tf_files=$(echo "${1}" | grep -P "\.((tf)|(tfvars))$")
    for file in ${tf_files}; do
        output=$(tflint "$file" 2>&1) || result=$?
        if [ "$result" -ne 0 ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${tf_files}" ]; then
        echo -e "tflint.............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "tflint.............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "tflint.............................................${green}Passed${nc}"
    fi
}

flake8_lint () {
    local status=0
    local logging=""

    py_files=$(echo "${1}" | grep -P "\.py$")
    for file in ${py_files}; do
        output=$(flake8 "$file" 2>&1) || result=$?
        if [ -n "$result" ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${py_files}" ]; then
        echo -e "flake8.............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "flake8.............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "flake8.............................................${red}Passed${nc}"
    fi
}

bandit_lint () {
    local status=0
    local logging=""

    py_files=$(echo "${1}" | grep -P "\.py$")
    for file in ${py_files}; do
        output=$(bandit -q "$file" 2>&1) || result=$?
        if [ "$result" -eq 1 ]; then
            logging=$(echo -e "${red} - ${file}${nc}\n${output}")
            status=1
        fi
    done

    if [ -z "${py_files}" ]; then
        echo -e "bandit.............................................${yellow}Skipped${nc}"
    elif [ "$status" -ne 0 ]; then
        echo -e "bandit.............................................${red}Failed${nc}"
        echo -e "${logging}"
        global_status=1
    else
        echo -e "bandit.............................................${red}Passed${nc}"
    fi
}

echo -e "Running pre-commit hook in '${branch_name}' branch..."

start_spinner ""

json_lint "${staged_files}"
yaml_lint "${staged_files}"
shell_lint "${staged_files}"
go_lint "${staged_files}"
es_lint "${staged_files}"
tf_lint "${staged_files}"
flake8_lint "${staged_files}"
bandit_lint "${staged_files}"

stop_spinner

exit $global_status
